# Nixos-crans

Le but de ce projet est d'expérimenter l'usage de nixos dans le cadre du crans.


## Commande de base
> Ces commandes supposent un environement linux ou darwin (macos).
 - `nix flake update` pour mettre a jour les dépendences.
 - `nix flake check` pour tester.
 - `nix fmt` pour formatter le code nix.