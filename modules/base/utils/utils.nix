{ config, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    acl
    curlMinimal
    emacs-nox
    lsb_release
    lsscsi
    molly-guard
    tree
    vlock
  ];
}
