{ config, pkgs, ... }:

{
  programs.less = {
    enable = true;
    envVariables = {
      #LESSHISTFILE = "/dev/null";
      #LESS = "-r";
    };
  };
}
