{
  imports = [
    ./bash.nix
    ./git.nix
    ./htop.nix
    ./less.nix
    ./nano.nix
    ./screen.nix
    ./tmux.nix
    ./utils.nix
    ./vim.nix
    ./zsh.nix
  ]
    }
