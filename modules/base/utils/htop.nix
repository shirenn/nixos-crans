{ config, pkgs, ... }:

{
  programs.htop = {
    enable = true;
    settings = {
      hide_threads = true;
      hide_userland_threads = true;
      show_program_path = "0";
      tree_view = true;
    };
  };
}
