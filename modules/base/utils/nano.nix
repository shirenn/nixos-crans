{ config, ... }:

{
  programs.nano = {
    syntaxHighlight = true;
    nanorc = ''
      			# Automatically indent a newly created line to the same number of tabs and/or
      			# spaces as the previous line (or as the next line if the previous line is the
      			# beginning of a paragraph).
      			set autoindent

      			# Set the target width for justifying and automatic hard-wrapping at this number
      			# of columns.  If the value is 0 or less, wrapping will occur at the width of
      			# the screen minus number columns, allowing the wrap point to vary along with
      			# the width of the screen if the screen is resized.  The default value is -8.
      			set fill 80

      			# Do regular-expression searches by default.  Regular expressions in nano are of
      			# the extended type (ERE).
      			set regexp

      			# Make the Home key smarter.  When Home is pressed anywhere but at the very
      			# beginning of non-whitespace characters on a line, the cursor will jump to that
      			# beginning (either forwards or backwards).  If the cursor is already at that
      			# position, it will jump to the true beginning of the line.
      			set smarthome

      			# Use a tab size of number columns.  The value of number must be greater than 0.
      			# The default value is 8.
      			set tabsize 4

      			# Remove trailing whitespace from wrapped lines when automatic hard-wrapping
      			# occurs or when text is justified.
      			set trimblanks
      		'';
  };
}
