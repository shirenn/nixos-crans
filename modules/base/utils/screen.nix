{ config, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    screen
  ];

  systemd.tmpfiles.rules = [
    "d /var/run/screen 0755 root utmp"
  ];

  # I didn't manage yet to change the ownership of screen
}
