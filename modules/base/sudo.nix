{ config, pkgs, ... }:

{
  security.sudo = {
    #enable = true;
    extraConfig = [
      #"Defaults		passprompt_override"
      #"Defaults		passprompt=\"[sudo] mot de passe pour %p sur %h: \""
    ];
    extraRules = [
      {
        groups = [ "_user" ];
        runAs = "root:ALL";
        commands = [ "NOPASSWD:/usr/bin/qm list" ];
      }
      {
        groups = [ "_nounou" ];
        commands = [ "ALL" ];
      }
    ];
  };
}
