{
  description = "Configurations nix du crans";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-23.05";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }: flake-utils.lib.eachDefaultSystem
    (system:
      let
        pkgs = import nixpkgs { inherit system; };

        fmt-check = pkgs.stdenvNoCC.mkDerivation {
          name = "fmt-check";
          src = ./.;

          nativeBuildInputs = with pkgs; [ nixpkgs-fmt ];
          phases = [ "installPhase" ];

          installPhase = ''
            nixpkgs-fmt --check $src
            touch $out
          '';
        };
      in
      {
        formatter = pkgs.nixpkgs-fmt;

        checks = {
          inherit fmt-check;
        };
      }) // { };
}
